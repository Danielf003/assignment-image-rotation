#include <stdio.h>

#include "bmp.h"
#include "util.h"

static char* readStatMssgs[] = {
    [READ_OK] = "Success!",
    [READ_INVALID_SIGNATURE]="Invalid arguments given to reading function",
    [READ_INVALID_BITS]="Pixel data loss occured while reading",
    [READ_INVALID_HEADER]="The header of read file is invalid",
    [READ_OPEN_ERROR]="Can not open that file to read"
};
static char* writeStatMssgs[] = {
    [WRITE_OK] = "Success!",
    [WRITE_INVALID_SIGNATURE]="Invalid arguments given to writing function",
    [WRITE_INVALID_BITS]="Pixel data loss occured while writing",
    [WRITE_INVALID_HEADER]="The header of write target file is invalid",
    [WRITE_OPEN_ERROR]="Can not open/create file to write",

};

void usage() {
    fprintf(stderr, "Usage: ./rotate BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    struct image img = {0};
    
    enum read_status readState = from_bmp_file(argv[1], &img);
    
    if (readState) {
        img_deallocate(&img);
        err("%s\n", readStatMssgs[readState]);
    }
    
    struct image rotated = img_rotate(img);
    
    //We can write different filename as a literal below instead of argv[1], 
    //but argv[2] as output destination is not implemented yet
    enum write_status writeState = to_bmp_file(argv[1], &rotated);
    
    if (writeState) {
        img_deallocate(&img);
        img_deallocate(&rotated);
        err("%s\n", writeStatMssgs[writeState]);
    }
    return 0;
}
