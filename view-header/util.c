#include "util.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

_Noreturn void err( const char* const msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}

