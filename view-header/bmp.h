#ifndef _BMP_H_
#define _BMP_H_

#include "image_tools.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_OPEN_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_SIGNATURE,
    WRITE_INVALID_BITS,
    WRITE_INVALID_HEADER,
    WRITE_OPEN_ERROR
};

enum read_status from_bmp_file( const char* filename, struct image* img );
enum write_status to_bmp_file(const char* filename , const struct image* img );

#endif
