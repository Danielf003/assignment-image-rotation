#ifndef _IMAGE_TOOLS_H_
#define _IMAGE_TOOLS_H_

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint32_t width;
  uint32_t height;
  struct pixel* data;
};

//void print_pix(const struct pixel* p, uint32_t w, uint32_t h);
void img_allocate(struct image* img, uint32_t w, uint32_t h);
void img_deallocate(struct image* img);
struct image img_rotate(struct image source);

#endif

