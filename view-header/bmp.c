#include "bmp.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header 
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

#define BMP_PLANES 1
#define BMP_TYPE 19778
#define BMP_BITMAP_INFO_SIZE (sizeof(struct bmp_header) - sizeof(uint16_t) - 3*sizeof(uint32_t))

static void print_header( const struct bmp_header* const header, FILE* const f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
}

static struct bmp_header generate_header( const uint32_t w, const uint32_t h ){
    struct bmp_header he = {0};
    he.bfType = BMP_TYPE;
    he.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel)*w*h + h*(w%4) ;
    he.bOffBits = sizeof(struct bmp_header);
    he.biSize = BMP_BITMAP_INFO_SIZE;
    he.biWidth = w;
    he.biHeight = h;
    he.biPlanes = BMP_PLANES;
    he.biBitCount = sizeof(struct pixel) * 8;
    he.biSizeImage = he.bfileSize - he.bOffBits;
    return he;
}

//static void bmp_header_update(struct bmp_header* const he, 
//        const uint32_t w, const uint32_t h){
//    
//    he->bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel)*w*h + h*(w%4) ;
//    he->biWidth = w;
//    he->biHeight = h;
//}

static bool read_header( FILE* const f, struct bmp_header* const header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

bool read_header_from_file( const char* const filename, 
        struct bmp_header* const header ) {
    
    if (!filename) return false;
    FILE* f = fopen( filename, "rb" ); 
    if (!f) return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true; 
    }

    fclose( f );
    return false;
}

static bool write_header( FILE* f, const struct bmp_header*const header ) {
    return fwrite( header, sizeof( struct bmp_header ), 1, f );
}



static enum read_status from_bmp( FILE* const in, struct image* const img){
    struct bmp_header he = {0};
    if (!read_header( in, &he )) return READ_INVALID_HEADER;
    fseek(in, sizeof(struct bmp_header), SEEK_SET);
    
    const uint32_t h = he.biHeight;
    const uint32_t w = he.biWidth;
    img_allocate(img, w, h);
    
    size_t accum = 0;
    struct pixel* addr =  img->data;
    for(size_t i = 0; i < h; i++){
        accum += fread(addr, sizeof(struct pixel)*w, 1, in );
        fseek(in, w%4, SEEK_CUR);
        addr += w;
    }
        
    if(accum < h){
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status from_bmp_file( const char* const filename, struct image* const img){
    if (!filename || !img) return READ_INVALID_SIGNATURE;
    FILE* f = fopen( filename, "rb" ); 
    if (!f) return READ_OPEN_ERROR;
    
    const enum read_status readstat = from_bmp( f, img );
    if (readstat) {
        fclose( f );
        return  readstat; 
    }
    fclose( f );
    return READ_OK;
}


static enum write_status to_bmp( FILE* const out, const struct image* const img ){
    const uint32_t h = img->height;
    const uint32_t w = img->width;
    const struct bmp_header he = generate_header(w, h);
    //comment line below if you do not want to output header of the result file
    print_header(&he, stdout);
    
    if(!write_header(out, &he)) return WRITE_INVALID_HEADER;
    fseek(out, sizeof(struct bmp_header), SEEK_SET);
    
    size_t accum = 0;
    const struct pixel* addr =  img->data;
    const char pad = 'q';
    for(size_t i = 0; i < h; i++){
        accum += fwrite( addr, sizeof(struct pixel)*w, 1, out);
        fwrite( &pad, sizeof(char), w%4, out);
        addr += w;
    }
    
    if(accum < h){
        return WRITE_INVALID_BITS;
    }
    return WRITE_OK;
}

enum write_status to_bmp_file(const char* const filename , const struct image* const img ){
    if (!filename || !img) return WRITE_INVALID_SIGNATURE;
    FILE* f = fopen( filename, "wb+" ); 
    if (!f) return WRITE_OPEN_ERROR;
    
    const enum write_status writestat = to_bmp( f, img );
    if (writestat) {
        fclose( f );
        return  writestat; 
    }
    fclose( f );
    return WRITE_OK;
}
