#include "image_tools.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//uncomment the function below if need to output raw 
//pixel values to console (ONLY FOR SMALL IMAGES)
/* 
void print_pix(struct pixel* p, uint32_t w, uint32_t h){
    for(size_t i=0; i<h; i++){
        for(size_t j=0; j<w; j++){
            printf("%"PRIu8".%"PRIu8".%"PRIu8" ", p[j+i*w].b, p[j+i*w].g, p[j+i*w].r);
        }printf("\n");
    }
}
*/

static void pixel_array_rotate(const struct pixel*const from, struct pixel* const to, 
        const uint32_t w_from, const uint32_t h_from){
    
    for(size_t i = 0; i < h_from; i++){
        for (size_t j = 0; j < w_from; ++j) {
            to[i + h_from*j] = from[w_from*(i+1) - j - 1];
        }
    }
}


void img_allocate(struct image* const img, const uint32_t w, const uint32_t h){
    if(!img) return;
    const size_t pixel_count = h * w;
    struct pixel* const pixarr = malloc(sizeof(struct pixel) * pixel_count);
    *img = (struct image) {w, h, pixarr};
}

void img_deallocate(struct image* const img){
    if(img && img->data)
        free(img->data);
}

struct image img_rotate(const struct image source){
    if(!source.data) return (struct image){0, 0, NULL};
    const uint32_t w = source.width;
    const uint32_t h = source.height;
    struct pixel* const copy = malloc(sizeof(struct pixel) * h * w);
    
    pixel_array_rotate(source.data, copy, w, h);
    return (struct image) {h, w, copy};
}